import numpy as np
from PIL import Image


def rgb_to_hsl(rgb_value):
    # normalize rgb values [0,1]
    _r = rgb_value[0] / 255
    _g = rgb_value[1] / 255
    _b = rgb_value[2] / 255

    x_max = np.amax([_r, _g, _b])
    x_min = np.amin([_r, _g, _b])

    chroma = x_max - x_min

    lightness = (x_max + x_min) / 2

    hue = 0
    if chroma == 0:
        hue = 0
    elif x_max == _r:
        hue = 60 * (((_g - _b) / chroma) % 6)
    elif x_max == _g:
        hue = 60 * ((_b - _r) / chroma + 2)
    elif x_max == _b:
        hue = 60 * ((_r - _g) / chroma + 4)

    if lightness == 0 or lightness == 1:
        saturation = 0
    else:
        saturation = chroma / (1 - np.abs(2 * lightness - 1))

    return hue, saturation, lightness


def convert(image_path, dest_path):
    image = Image.open(image_path)
    width, height = image.size
    pixels = image.load()

    if image.mode == 'RGB':
        pixels_gray = np.zeros((height, width, 3), dtype=np.uint8)
        for x in range(width):
            for y in range(height):
                lightness = rgb_to_hsl(pixels[x, y])[2]  # lightness is given in percentage [0,1]
                val = int(lightness * 255)
                pixels_gray[y, x] = [val, val, val]  # make a gray value
        image_gray = Image.fromarray(pixels_gray, "RGB")
        image_gray.save(dest_path)



