import numpy as np
from PIL import Image


def calculate_luminance(rgb_value):
    # normalize rgb_values
    _rgb = np.array(rgb_value) / 255
    # convert gamma-compressed rgb values to linear rgb
    for i in range(_rgb.size):
        val = _rgb[i]
        if val <= 0.04045:
            _rgb[i] = 25 * val / 323
        else:
            _rgb[i] = ((200 * val + 11) / 211) ** (12 / 5)

    # calculate (relative/perceptive) luminance (Y)
    luminance = 0.2126 * _rgb[0] + 0.7152 * _rgb[1] + 0.0722 * _rgb[2]
    return luminance


def convert(image_path, dest_path):
    image = Image.open(image_path)
    width, height = image.size
    pixels = image.load()

    if image.mode == 'RGB':
        pixels_gray = np.zeros((height, width, 3), dtype=np.uint8)
        for x in range(width):
            for y in range(height):
                luminance = calculate_luminance(pixels[x, y])
                val = int(luminance * 255)
                pixels_gray[y, x] = [val, val, val]  # make a gray value
        image_gray = Image.fromarray(pixels_gray, "RGB")
        image_gray.save(dest_path)