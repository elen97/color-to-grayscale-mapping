import cv2
import numpy as np

# color RED limits
LOW_RED = []
HIGH_RED = []
# color ORANGE limits
LOW_ORANGE = []
HIGH_ORANGE = []
# color YELLOW limit
LOW_YELLOW = []
HIGH_YELLOW = []
# color GREEN limits
LOW_GREEN = []
HIGH_GREEN = []
# color BLUE limits
LOW_BLUE = []
HIGH_BLUE = []
# color BLACK limits
LOW_BLACK = []
HIGH_BLACK = []

# function for saturation and value border
# saturation = ((320/169)*value**2)-(640/169)*value + (1769/845)
# in range of opencv: saturation = ((64/8619)*value**2)-(640/169)*value+533.8402367
for value in range(256):
    matching_saturation = 256
    for saturation in range(256):
        if saturation == np.ceil(((64/8619)*value**2)-(640/169)*value+533.8402367):
            matching_saturation = saturation
            LOW_RED.append([0, saturation, value])
            HIGH_RED.append([12, 255, 255])
            LOW_RED.append([145, saturation, value])
            HIGH_RED.append([179, 255, 255])
            LOW_ORANGE.append([13, saturation, value])
            HIGH_ORANGE.append([20, 255, 255])
            LOW_YELLOW.append([21, saturation, value])
            HIGH_YELLOW.append([40, 255, 255])
            LOW_GREEN.append([41, saturation, value])
            HIGH_GREEN.append([82, 255, 255])
            LOW_BLUE.append([83, saturation, value])
            HIGH_BLUE.append([144, 255, 255])

            # when it was at the border, we can break the inner loop
            break

    if value >= 204:
        # it is only black with value < 205
        HIGH_BLACK.append([179, matching_saturation - 1, 204])
    else:
        HIGH_BLACK.append([179, matching_saturation - 1, value])
    LOW_BLACK.append([0, 0, 0])

LOW_RED = np.array(LOW_RED)
HIGH_RED = np.array(HIGH_RED)
LOW_ORANGE = np.array(LOW_ORANGE)
HIGH_ORANGE = np.array(HIGH_ORANGE)
LOW_YELLOW = np.array(LOW_YELLOW)
HIGH_YELLOW = np.array(HIGH_YELLOW)
LOW_GREEN = np.array(LOW_GREEN)
HIGH_GREEN = np.array(HIGH_GREEN)
LOW_BLUE = np.array(LOW_BLUE)
HIGH_BLUE = np.array(HIGH_BLUE)
LOW_BLACK = np.array(LOW_BLACK)
HIGH_BLACK = np.array(HIGH_BLACK)


def detect_color(img_rgb, lower, higher, transformation=True):
    img_hsv = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2HSV)

    mask = 0
    for i in range(len(lower)):
        mask_i = cv2.inRange(img_hsv, lower[i], higher[i])

        mask = cv2.bitwise_or(mask, mask_i)

    if transformation:
        #mask = cv2.erode(mask, np.ones((2, 2), np.uint8))
        kernel = np.ones((3, 3), np.uint8)
        #mask = cv2.dilate(mask, kernel)
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

    mask_inv = cv2.bitwise_not(mask)
    res = cv2.bitwise_and(img_rgb, img_rgb, mask=mask_inv)

    return res


def detect_red(img_rgb):
    return detect_color(img_rgb, LOW_RED, HIGH_RED)


def detect_orange(img_rgb):
    return detect_color(img_rgb, LOW_ORANGE, HIGH_ORANGE)


def detect_yellow(img_rgb):
    return detect_color(img_rgb, LOW_YELLOW, HIGH_YELLOW)


def detect_green(img_rgb):
    return detect_color(img_rgb, LOW_GREEN, HIGH_GREEN)


def detect_blue(img_rgb):
    return detect_color(img_rgb, LOW_BLUE, HIGH_BLUE)


def detect_black(img_rgb):
    return detect_color(img_rgb, LOW_BLACK, HIGH_BLACK, False)
