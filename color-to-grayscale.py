import cv2
import color_area_detection
import numpy as np

img = cv2.imread('pics/color_example.jpg')
red_pixels = color_area_detection.detect_red(img)
orange_pixels = color_area_detection.detect_orange(img)
yellow_pixels = color_area_detection.detect_yellow(img)
green_pixels = color_area_detection.detect_green(img)
blue_pixels = color_area_detection.detect_blue(img)
black_pixels = color_area_detection.detect_black(img)

colors = []
if blue_pixels[blue_pixels == [0, 0, 0]].size > 1:
    colors.append(blue_pixels)
if green_pixels[green_pixels == [0, 0, 0]].size > 1:
    colors.append(green_pixels)
if yellow_pixels[yellow_pixels == [0, 0, 0]].size > 1:
    colors.append(yellow_pixels)
if orange_pixels[orange_pixels == [0, 0, 0]].size > 1:
    colors.append(orange_pixels)
if red_pixels[red_pixels == [0, 0, 0]].size > 0:
    colors.append(red_pixels)

# make whole picture white
img[:] = [255, 255, 255]

# only use rgb values [80,80,80] until [255,255,255] i.e. a range of 175
for i in range(len(colors)):
    grayvalue = 255 - int((175 / len(colors)) * (i + 1))
    img[np.where((colors[i] == [0, 0, 0]).all(axis=2))] = [grayvalue, grayvalue, grayvalue]

# override black pixels
img[np.where((black_pixels == [0, 0, 0]).all(axis=2))] = [0, 0, 0]

cv2.imshow("magic", img)

cv2.waitKey(10000)
